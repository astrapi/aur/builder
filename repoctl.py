#!/usr/bin/env python3

import subprocess
import click
import numpy
from sty import fg, bg
from lib.config import Config
from lib.package import Package
import lib.globals as Globals
from lib.pkgbuild import Pkgbuild
import xml.etree.ElementTree as ET

@click.group()

def cli():
    pass

@cli.command("add", help="add package")
@click.argument('package')
@click.option("--git", type=click.Choice(['aur', 'custom'], case_sensitive=False), help="package git")
def cli_add(package, git):
    if not git:
        git = "aur"
    add(package, git)

@cli.command("remove", help="remove package")
@click.argument('package')
def main_remove(package):
    remove(package)

@cli.command("build", help="build package")
#@click.option("--package", required=True, help="package name or id")
@click.argument('package')
def main_build(package):
    build(package)

@cli.command("update", help="update package list")
def main_update():
    update()

@cli.command("upgrade", help="build update package")
def main_upgrade():
    upgrade()

@cli.command("list", help="list packages")
@click.option("--builded", help="list builded packages", is_flag=True)
@click.option("--upgradable", help="list upgradable packages", is_flag=True)
def main_list(package):
    pass

# main = click.Group(help="repoctl")

# @click.option('--debug/--no-debug', default=False)

# @main.command("add", help="add package")
# @click.argument('package')
# @click.option("-g", "--git", type=click.Choice(['aur', 'custom'], case_sensitive=False), help="package git")
# def main_add(package, git):
#     if not git:
#         git = "aur"
#     add(package, git)

# @main.command("remove", help="remove package")
# @click.argument('package')
# def main_remove(package):
#     remove(package)

# @main.command("build", help="build package")
# #@click.option("--package", required=True, help="package name or id")
# @click.argument('package')
# def main_build(package):
#     build(package)

# @main.command("update", help="update package list")
# def main_update():
#     pass

# @main.command("upgrade", help="build update package")
# def main_upgrade():
#     pass

# @main.command("list", help="list packages")
# @click.option("--builded", help="list builded packages", is_flag=True)
# @click.option("--upgradable", help="list upgradable packages", is_flag=True)
# def main_list(package):
#     pass

def add(package, git):
    package = Package(package, git)
    package.add()

def remove(package):
    package = Package(package)
    package.remove()

def build(package):
    package = Package(package)
    package.build()

def update1():
    #package = Package()
    #package.update()
    print("update")

    tree = ET.parse(Globals.package_file)
    __xml_root = tree.getroot()

    __upgrade = []
    for item in __xml_root.findall('package'):
        pkgbuild = Pkgbuild(item.get("name"), item.get("git"))

        __equal_local = item.get("pkgver") + "-" + item.get("pkgrel")
        __equal_remote = pkgbuild.get_version() + "-" + pkgbuild.get_release()

        if __equal_local != __equal_remote:
            print(item.get("name") + "(" + item.get("arch") + "):",__equal_local, "->", __equal_remote)
            __upgrade.append(item.get("id"))

    print(__upgrade)
    numpy.save("upgrade", __upgrade)

def update():
    tree = ET.parse(Globals.package_file)
    __xml_root = tree.getroot()

    __upgrade_ids = numpy.array([0])
    __upgrade_ids = numpy.delete(__upgrade_ids, numpy.where(__upgrade_ids == 0))
    for item in __xml_root.findall('package'):
        pkgbuild = Pkgbuild(item.get("name"), item.get("git"))

        __equal_local = item.get("pkgver") + "-" + item.get("pkgrel")
        __equal_remote = pkgbuild.get_version() + "-" + pkgbuild.get_release()

        if __equal_local != __equal_remote:
            print(item.get("name") + "(" + item.get("arch") + "):",__equal_local, "->", __equal_remote)
            __upgrade_ids = numpy.append(__upgrade_ids, item.get("id"))
    
    numpy.save("upgrade", __upgrade_ids)

def upgrade():
    #__package = Package()
    __upgrade_ids = numpy.load("upgrade.npy")
    for __upgrade_id in __upgrade_ids:
        #print(__upgrade_id)
        #__package.do_build(__upgrade_id)
        build(__upgrade_id)
        __upgrade_ids = numpy.delete(__upgrade_id, numpy.where(__upgrade_ids == __upgrade_id))

    str_rm = ["rm", "upgrade.npy"]
    result = subprocess.run(str_rm, capture_output=True, text=True, check=True)

def upgrade1():
    #package = Package()
    #package.upgrade()
    __upgrade = numpy.load("upgrade.npy")
    print(__upgrade)
    #__upgrade.remove(63)
    #print(__upgrade)
    #for upgrade in __upgrade:
    #    print(upgrade)
   #     new_array = numpy.delete(__upgrade, numpy.where(__upgrade == 63))
        #self.do_build(upgrade)

if __name__ == "__main__":
    Globals.config_file = "config.xml"
    Globals.package_file = "packages.xml"
    #exit(main())
    cli()


# import argparse
# import sys
# from sty import fg, bg, ef, rs
# from lib.functions import *
# from lib.config import Config
# from lib.package import Package
# import lib.globals as g

# def main_old():
#     package = Package('packages.xml')

#     if(len(sys.argv) == 1):
#         print(bg.red, "Kein Argument angegeben!", bg.rs)
#         return
#     elif(sys.argv[1] == "add"):
#         try:
#             #add(sys.argv[2], sys.argv[3])
#             add()
#         except IndexError:
#             print(bg.red, "Kein Packet angegeben!", bg.rs)
#     elif(sys.argv[1] == "remove"):
#         try:
#             remove(sys.argv[2])
#         except IndexError:
#             print(bg.red, "Kein Packet angegeben!", bg.rs)
#     elif(sys.argv[1] == "build"):
#         try:
#             build(sys.argv[2])
#         except IndexError:
#             print(bg.red, "Kein Packet angegeben!", bg.rs)
#     elif(sys.argv[1] == "update"):
#         update()
#     else:
#         print(bg.red, "Unbekanntes Argument angegeben!", bg.rs)
#         return
    
#     conf = Config('config.xml')
#     #print(conf.get_repo_path('amd64'))
#     g.config_file = "Hallo Welt!"  


# def add():
#     package = Package('packages.xml')
#     package.add(sys.argv[2], sys.argv[3])

# main()
    