#!/usr/bin/env python3

import subprocess
import os
import sys
from urllib.request import urlopen
import xml.etree.ElementTree as ET
import requests
from sty import fg, bg, ef, rs
from subprocess import PIPE, run

class G:
    run_dir = os.path.dirname(__file__)
    conf_dir = run_dir[0:-5] + "conf"
    build_dir = run_dir[0:-5] + "build"
    repo_path_1 = None
    repo_path_2 = None
    git_url_1 = None
    git_url_2 = None

class pkg:
    package_id = None
    name = None
    version_local = None
    version_remote = None
    release_local = None
    release_remote = None
    arch = None
    git = None
    file = None
    pkgbuild_url = None
    git_url = None

def main():
    tree = ET.parse(G.conf_dir + '/config.xml')
    root = tree.getroot()

    for item in root.findall('.//repo'):
        if(item.get('arch') == 'amd64'):
            G.repo_path_1 = item.get('path')
        elif(item.get('arch') == 'arm64v8'):
            G.repo_path_2 = item.get('path')

    for item in root.findall('.//git'):
        if(item.get('name') == 'aur'):
            G.git_url_1 = item.get('url')
        elif(item.get('name') == 'astrapi'):
            G.git_url_2 = item.get('url')

    if(len(sys.argv) == 1):
        print("Kein Argument angegeben!")
        return
    elif(sys.argv[1] == "add"):
        try:
            add(sys.argv[2], sys.argv[3])
        except IndexError:
            print("Kein Packet angegeben!")
    elif(sys.argv[1] == "remove"):
        try:
            remove(sys.argv[2])
        except IndexError:
            print("Kein Packet angegeben!")
    elif(sys.argv[1] == "build"):
        try:
            build(sys.argv[2])
        except IndexError:
            print("Kein Packet angegeben!")
    elif(sys.argv[1] == "update"):
        update()
    elif(sys.argv[1] == "repo"):
        run_repo_add()
    elif(sys.argv[1] == "test"):
        print("test")
      #requests.post("https://notify.simpsons.lan/repo", data="Backup successful".encode(encoding='utf-8'))
    else:
        print("Unbekanntes Argument angegeben!")
        return
    
    #str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_1 + ":/repo", "repo-add:x86_64" ,"astrapi"]
    #subprocess.call(str_docker)
    
    #str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_2 + ":/repo", "repo-add:aarch64" ,"astrapi"]
    #subprocess.call(str_docker)

def update():
    print(bg.grey, "check for updates ...", bg.rs)

    tree = ET.parse(G.conf_dir + '/packages.xml')
    root = tree.getroot()

    for item in root.findall('package'):
        pkg.name = item.get('name')
        pkg.package_id = item.get('id')
        pkg.git = item.get('git')
        pkg.file = item.get('file')
        pkg.arch = item.get('arch')
        pkg.pkgbuild_url = get_pkgbuild_url(pkg.name, pkg.git)
        pkg.version_local = item.get('pkgver')
        pkg.release_local = item.get('pkgrel')

        data = urlopen(pkg.pkgbuild_url).read().decode('utf-8')
        for line in data.split('\n'):
            row = line.split('=')
            if row[0] == '_pkgver':
                _pkgver_remote = row[1]
            elif row[0] == 'pkgver':
                if row[1] == "$_pkgver":
                    pkg.version_remote = _pkgver_remote
                else:
                    pkg.version_remote = row[1]
            elif row[0] == 'pkgrel':
                pkg.release_remote = row[1]

        equal_local = pkg.version_local + "_" + pkg.release_local
        equal_remote = pkg.version_remote + "_" + pkg.release_remote

        if pkg.version_local != "0":
            if equal_local != equal_remote:
                print(bg.grey, pkg.package_id, pkg.name, "|", pkg.version_local, bg.rs)
                build(pkg.package_id)

    run_repo_add()

def build(param):
    flag = True
    try:
        int(param)
    except ValueError:
        flag = False

    if flag:
        #pass
        do_build(param)
    else:
        tree = ET.parse(G.conf_dir + '/packages.xml')
        root = tree.getroot()

        for item in root.findall('package'):
            if item.get('name') == param:
                #pass
                do_build(item.get('id'))
                
    run_repo_add()
                
def do_build(package_id):
    print(package_id)
    tree = ET.parse(G.conf_dir + '/packages.xml')
    root = tree.getroot()
    
    for item in root.findall("./package/[@id='" + package_id  + "']"):
        pkg.name = item.get('name')
        pkg.file = item.get('file')
        pkg.arch = item.get('arch')
        pkg.git = item.get('git')
        pkg.git_url = get_git_url(pkg.name, pkg.git)
        pkg.pkgbuild_url = get_pkgbuild_url(pkg.name, pkg.git)
        
        data = urlopen(pkg.pkgbuild_url).read().decode('utf-8')
        for line in data.split('\n'):
            row = line.split('=')
            if row[0] == '_pkgver':
                _pkgver_remote = row[1]
            elif row[0] == 'pkgver':
                if row[1] == "$_pkgver":
                    pkg.version_remote = _pkgver_remote
                else:
                    pkg.version_remote = row[1]
            elif row[0] == 'pkgrel':
                pkg.release_remote = row[1]
        
        # neues Paket bauen
        str_docker = ["docker", "run", "--name", "makepkg", "--user", "makepkg", "--net", "host", "makepkg:" + get_docker_image(pkg.arch), pkg.name, pkg.git_url]
        subprocess.call(str_docker)
        print(bg.grey,"build", bg.rs)
        
        # Paket aus docker koieren
        str_docker = ["docker", "cp", "makepkg:/home/makepkg/pkg/.", G.build_dir]
        subprocess.call(str_docker)
        print(bg.grey,"copy data", bg.rs)
        
        # container löschen
        str_docker = ["docker", "rm", "makepkg"]
        subprocess.call(str_docker)
        print(bg.grey,"delete container", bg.rs)
        
        # Altes Paket löschen (wenn vorhanden)
        old_file_1 = G.repo_path_1 + "/" + pkg.file
        if os.path.exists(old_file_1): 
            os.remove(old_file_1)
            print(bg.grey, " emove file ", old_file_1, bg.rs)
        old_file_2 = G.repo_path_2 + "/" + pkg.file
        if os.path.exists(old_file_2):
            os.remove(old_file_2)
            print(bg.grey, "remove file ", old_file_2, bg.rs)
        
        for file in os.listdir(G.build_dir ):
            # neues Paket verschieben/kopieren
            if(pkg.arch == "any"):
                os.popen( "cp " + G.build_dir + "/" + file +" " + G.repo_path_1 + "/" + file)
                print(bg.grey, "cp ", G.repo_path_1 , "/" , file, bg.rs)
                os.popen( "mv " + G.build_dir + "/" + file +" " + G.repo_path_2 + "/" + file)
                print(bg.grey, "cp ", G.repo_path_2 , "/" , file, bg.rs)
            elif(pkg.arch == "x86_64"):
                os.popen( "mv " + G.build_dir + "/" + file +" " + G.repo_path_1 + "/" + file)
                print(bg.grey, "cp ", G.repo_path_1 , "/" , file, bg.rs)
            elif(pkg.arch == "aarch64"):
                os.popen( "mv " + G.build_dir + "/" + file +" " + G.repo_path_2 + "/" + file)
                print(bg.grey, "cp ", G.repo_path_2 , "/" , file, bg.rs)

            # Metadaten schreiben
            item.set('file', file)
            item.set('pkgver', pkg.version_remote)
            item.set('pkgrel', pkg.release_remote)
            tree.write(G.conf_dir + '/packages.xml')
            print(bg.grey, "write metadata", bg.rs)

def run_repo_add():
    str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_1 + ":/repo", "repo-add:x86_64" ,"astrapi"]
    result = run(str_docker, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    print(result.returncode, result.stdout, result.stderr)


    str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_2 + ":/repo", "repo-add:aarch64" ,"astrapi"]
    result = run(str_docker, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    print(result.returncode, result.stdout, result.stderr)


    #str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_1 + ":/repo", "repo-add:x86_64" ,"astrapi"]
    #subprocess.call(str_docker)
    
    #str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", G.repo_path_2 + ":/repo", "repo-add:aarch64" ,"astrapi"]
    #subprocess.call(str_docker)

def get_docker_image(arch):
    if(arch == 'any'):
        return 'x86_64'
    else:
        return arch

def get_pkgbuild_url(package, git):
    if (git == "astrapi"):
        return G.git_url_2 + package + '/-/raw/main/PKGBUILD'
    else:
        return G.git_url_1 + 'cgit/aur.git/plain/PKGBUILD?h=' + package

def get_git_url(package, git):
    if (git == "astrapi"):
        return G.git_url_2 + package + '.git'
    else:
        return G.git_url_1 + package + '.git'

def get_pkgbuild_arch(package, git):
    pkgbuild_url = get_pkgbuild_url(package, git)
        
    data = urlopen(pkgbuild_url).read().decode('utf-8')
    for line in data.split('\n'):
        row = line.split('=')
        if row[0] == 'arch':
            archs = row[1][1:-1]
            return archs

def add(package, git):
    tree = ET.parse(G.conf_dir + '/packages.xml')
    root = tree.getroot()

    count = 0
    for item in root.findall('package'):
        count = count + 1
        if(item.get('name') == package):
            print("Package bereits vorhanden")
            return

    archs = get_pkgbuild_arch(package, git)
    package_id = str(count + 1)
    a = ["any", "x86_64", "aarch64"]
    for arch_ in archs.split(' '):
        arch = arch_.strip("'")
        if arch in a:
            new = ET.Element("package")
            new.set('id', package_id)
            new.set('name', package)
            new.set('pkgver', "0")
            new.set('pkgrel', "0")
            new.set('git', git)
            new.set('file', "null")
            new.set('arch', arch)
            root.append(new)

            prettify(root)
            ET.ElementTree(root).write(G.conf_dir + '/packages.xml',encoding="utf-8")
            print(fg.green + "Add package" + fg.li_blue, package + "(" + arch + ")", fg.green, "with id", package_id ,"successfull.", fg.rs)
            #do_build(package_id)

#    build(package)
#    run_repo_add()

def remove(param):
    flag = True
    try:
        int(param)
    except ValueError:
        flag = False

    if flag:
        #pass
        do_remove(param)
    else:
        tree = ET.parse(G.conf_dir + '/packages.xml')
        root = tree.getroot()

        for item in root.findall('package'):
            if item.get('name') == param:
                #pass
                do_remove(item.get('id'))
                
    run_repo_add()

def do_remove(package_id):
    tree = ET.parse(G.conf_dir + '/packages.xml')
    root = tree.getroot()
    
    for item in root.findall("./package/[@id='" + package_id  + "']"):
        print(item.get('id'))
        pkg.file = item.get('file')
        
        # Paket löschen (wenn vorhanden)
        old_file_1 = G.repo_path_1 + "/" + pkg.file
        if os.path.exists(old_file_1): 
            os.remove(old_file_1)
            print(bg.grey, " emove file ", old_file_1, bg.rs)
        old_file_2 = G.repo_path_2 + "/" + pkg.file
        if os.path.exists(old_file_2):
            os.remove(old_file_2)
            print(bg.grey, "remove file ", old_file_2, bg.rs)
    
        root.remove(item)
        prettify(root)
        ET.ElementTree(root).write(G.conf_dir + '/packages.xml',encoding="utf-8")
    
def prettify(elem, indent="  ", level=0):
    i = "\n" + level*indent
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + indent
        for el in elem:
            prettify(el, indent, level+1)
        if not el.tail or not el.tail.strip():
            el.tail = i
    if not elem.tail or not elem.tail.strip():
        elem.tail = i

if __name__ == "__main__":
    main();
