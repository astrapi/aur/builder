import os
import subprocess
import sys
import xml.etree.ElementTree as ET
import numpy
from sty import fg, bg
from lib.functions import prettify
import lib.globals as Globals
from lib.config import Config
from lib.pkgbuild import Pkgbuild

class Package:
    def __init__(self, package = None, git = None):
        self.__conf = Config(Globals.config_file)

        tree = ET.parse(Globals.package_file)
        self.__xml_root = tree.getroot()
        self.__git = git
        self.__package = package
        self.__archs = self.__conf.get_archs()
        #self.__upgrade = []

        if git:
            self.__pkgbuild = Pkgbuild(self.__package, self.__git)
            self.__pkgbuild_archs = self.__pkgbuild.get_archs()

    def add(self):
        print(bg.da_green, "Add", self.__package, "to package list ...", bg.rs)    

        for item in self.__xml_root.findall('package'):
            if(item.get('name') == self.__package):
                print(bg. da_red, "Package bereits vorhanden", bg.rs)
                return

        print(self.__pkgbuild_archs)

        for arch in self.__pkgbuild_archs.split(' '):
            if arch in self.__archs:
                new = ET.Element("package")
                new.set('id', str(int(self.__xml_root.attrib.get('id')) + 1))
                new.set('name', self.__package)
                new.set('pkgver', "0")
                new.set('pkgrel', "0")
                new.set('git', self.__git)
                new.set('file', "null")
                new.set('arch', arch)
                self.__xml_root.append(new)

                prettify(self.__xml_root)
                self.__xml_root.set('id', str(int(self.__xml_root.attrib.get('id')) + 1))
                ET.ElementTree(self.__xml_root).write(Globals.package_file, encoding="utf-8")

                print(bg.da_green, "Added", self.__package, "with id",str(int(self.__xml_root.attrib.get('id')) + 1), "(" + arch + ")" ,bg.rs)

    def remove(self):
        print(bg.da_magenta, "Remove", self.__package, bg.rs)

        flag = True
        try:
            int(self.__package)
        except ValueError:
            flag = False

        if flag:
            #remove by id
            self.do_remove(self.__package)
        else:
            for item in self.__xml_root.findall('package'):
                if(item.get('name') == self.__package):
                    #remove by name
                    self.do_remove(item.get('id'))

    def do_remove(self, package_id):
        for item in self.__xml_root.findall("./package/[@id='" + package_id  + "']"):
            print(bg.da_cyan, "Remove id", item.get('id'), bg.rs)

            self.__xml_root.remove(item)
            prettify(self.__xml_root)
            ET.ElementTree(self.__xml_root).write(Globals.package_file, encoding="utf-8")
        #  pkg.file = item.get('file')
        
        # # Paket löschen (wenn vorhanden)
        # old_file_1 = G.repo_path_1 + "/" + pkg.file
        # if os.path.exists(old_file_1): 
        #     os.remove(old_file_1)
        #     print(bg.grey, " emove file ", old_file_1, bg.rs)
        # old_file_2 = G.repo_path_2 + "/" + pkg.file
        # if os.path.exists(old_file_2):
        #     os.remove(old_file_2)
        #     print(bg.grey, "remove file ", old_file_2, bg.rs)
    
        # root.remove(item)
        # prettify(root) """
        # ET.ElementTree(root).write(G.conf_dir + '/packages.xml',encoding="utf-8")

    def build(self):
        print(bg.da_magenta, "Build", self.__package, bg.rs)

        flag = True
        try:
            int(self.__package)
        except ValueError:
            flag = False

        if flag:
            #build by id
            self.do_build(self.__package)
        else:
            for item in self.__xml_root.findall('package'):
                if(item.get('name') == self.__package):
                    #build by name
                    #self.do_build(item.get('id'))
                    self.repo_add(item.get('arch'))

    def do_build(self, package_id):
        for item in self.__xml_root.findall("./package/[@id='" + package_id  + "']"):
            git_url = self.__conf.get_git_url(item.get("git")) + self.__package + ".git"

            # neues Paket bauen
            print(bg.da_cyan, "Build id", item.get('id'), bg.rs)
            str_docker = ["docker", "run", "--name", "makepkg", "--user", "makepkg", "--net", "host", "makepkg:" + self.__conf.get_docker_image(item.get('arch')), self.__package, git_url]
            result = subprocess.run(str_docker, capture_output=True, text=True, check=True)

            # Paket aus docker koieren
            print(bg.da_cyan, "copy data", bg.rs)
            str_docker = ["docker", "cp", "makepkg:/home/makepkg/pkg/.", "build"]
            result = subprocess.run(str_docker, capture_output=True, text=True, check=True)
            
            # container löschen
            print(bg.da_cyan, "delete container", bg.rs)
            str_docker = ["docker", "rm", "makepkg"]
            result = subprocess.run(str_docker, capture_output=True, text=True, check=True)
            # # print("stdout:", result.stdout)
            # # print("stderr:", result.stderr)

            # Altes Paket löschen (wenn vorhanden)
            for arch in self.__archs:
                if not arch == "any":
                    file = self.__conf.get_storage_path(arch) + "/" + item.get("file")
                    if os.path.exists(file):
                        print(bg.da_cyan, "Remove", file, bg.rs)
                        os.remove(file)

            # datei ermitteln
            for file in os.listdir("build"):
                if "pkg.tar" in file:
                    self.__file = file
                    print(self.__file)

            # neues Paket verschieben/kopieren
            if item.get("arch") == "any":
                for arch in self.__archs:
                    if not arch == "any":
                        self.copy_file(arch)
            else:
                self.copy_file(item.get("arch"))

            # build leeren
            str_rm = ["rm", "build/" + self.__file]
            result = subprocess.run(str_rm, capture_output=True, text=True, check=True)

            #metadaten schreiben
            pkgbuild = Pkgbuild(item.get("name"), item.get("git"))
            item.set('file', self.__file)
            item.set('pkgver', pkgbuild.get_version())
            item.set('pkgrel', pkgbuild.get_release())
            ET.ElementTree(self.__xml_root).write(Globals.package_file, encoding="utf-8")
            print(bg.grey, "write metadata", bg.rs)
            
    # def update(self):
    #     print("update")

    #     self.__upgrade = []
    #     for item in self.__xml_root.findall('package'):
    #         pkgbuild = Pkgbuild(item.get("name"), item.get("git"))

    #         __equal_local = item.get("pkgver") + "-" + item.get("pkgrel")
    #         __equal_remote = pkgbuild.get_version() + "-" + pkgbuild.get_release()

    #         if __equal_local != __equal_remote:
    #             print(item.get("name") + "(" + item.get("arch") + "):",__equal_local, "->", __equal_remote)
    #             self.__upgrade.append(item.get("id"))

    #     numpy.save("upgrade", self.__upgrade)

    # def upgrade(self):
        # self.__upgrade = numpy.load("upgrade.npy")
        # for upgrade in self.__upgrade:
        #     self.do_build(upgrade)

    def copy_file(self, arch):
        str_copy = ["cp", "build/" + self.__file, self.__conf.get_storage_path(arch)]
        result = subprocess.run(str_copy, capture_output=True, text=True, check=True)

    def repo_add(self, arch):

        if arch == "any":
            pass
        else:
            str_docker = ["docker", "run", "--rm", "--user", "1000:1000", "--volume", self.__conf.get_storage_path(arch) + ":/repo", "repo-add:" + arch ,"astrapi"]
            result = subprocess.run(str_docker, capture_output=True, text=True, check=True)