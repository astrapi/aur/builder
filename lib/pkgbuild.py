from urllib.request import urlopen
from lib.config import Config
import lib.globals as Globals
from sty import fg, bg

class Pkgbuild:
    def __init__(self, package, git):
        #print(fg.da_red, bg.grey , "DEBUG", "__init__", fg.rs, bg.rs)
        self.__conf = Config(Globals.config_file)
        self.__git = git
        self.__git_url = self.__conf.get_git_url(self.__git)
        self.__package = package

        if (self.__git == "custom"):
            self.__url =  self.__git_url + self.__package + '/-/raw/main/PKGBUILD'
        else:
            self.__url = self.__git_url + 'cgit/aur.git/plain/PKGBUILD?h=' + self.__package
        
        data = urlopen(self.__url).read().decode('utf-8')
        for line in data.split('\n'):
            row = line.split('=')
            if row[0] == 'arch':
               self.__archs = row[1][1:-1].replace("'", "")
            if row[0] == '_pkgver':
                _pkgver_remote = row[1]
            elif row[0] == 'pkgver':
                if row[1] == "$_pkgver":
                    self.__version = _pkgver_remote
                else:
                    self.__version = row[1]
            elif row[0] == 'pkgrel':
                self.__release = row[1]

    def get_archs(self):
        return self.__archs

    def get_url(self):
        return self.__url

    def get_version(self):
        return self.__version

    def get_release(self):
        return self.__release

