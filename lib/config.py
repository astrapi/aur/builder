from urllib.request import urlopen
import xml.etree.ElementTree as ET

class Config:
    
    def __init__(self, config_file = None):
        tree = ET.parse(config_file)
        self.__xml_root = tree.getroot()
    
    def get_storage_path(self, arch):
        for item in self.__xml_root.findall('.//storage'):
            if(item.get('arch') == arch):
                return item.get('path')
            
    def get_archs(self):
        r = [] 
        for item in self.__xml_root.findall('.//arch'):
            r.append(item.get('name'))
        return r

    def get_git_url(self, name):
        for item in self.__xml_root.findall('.//url'):
            if(item.get('name') == name):
                return item.get('url') 
                 
    def get_docker_image(self, arch):
        if(arch == 'any'):
            return 'x86_64'
        else:
            return arch